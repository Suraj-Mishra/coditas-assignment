import React from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faArrowDown, faArrowUp } from '@fortawesome/free-solid-svg-icons';
import FirstPage from './FirstPage';

library.add(faArrowDown, faArrowUp);

function App() {
  return (
    <div className="App">
      <FirstPage />
    </div>
  );
}

export default App;
