import React, { useState, useEffect } from 'react';
import FirstPageHelper from './FirstPageHelper';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './FirstPage.css';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    minWidth: 300,
  },
});

const FirstPage = () => {
  const [data, setData] = useState<Object>();

  let time = 0;
  let arr: Array<Object> = [];

  useEffect(() => {
    async function result() {
      const fetchedData = await FirstPageHelper();
      setData(fetchedData);
    }
    result();
  }, [time]);

  setInterval(() => {
    time = time += 1;
    if (data !== undefined) {
      arr.push(data);
    }
  }, 5000);

  const classes = useStyles();
  return (
    <div className="div-container">
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Time</TableCell>
              <TableCell align="right">Euro(&euro;)</TableCell>
              <TableCell align="right">Pound(&pound;)</TableCell>
              <TableCell align="right">USD(&#36;)</TableCell>
              <TableCell align="right">Change</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <>
              {arr.forEach((row: any) => (
                <TableRow>
                  <TableCell component="th" scope="row">
                    {row.time}
                  </TableCell>
                  <TableCell align="right">{row}</TableCell>
                  <TableCell align="right">{row}</TableCell>
                  <TableCell align="right">{row}</TableCell>
                  <TableCell align="right">{row}</TableCell>
                </TableRow>
              ))}
            </>
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
};

export default FirstPage;
