const FirstPageHelper = async () => {
  const result = await fetch(
    'https://api.coindesk.com/v1/bpi/currentprice.json',
  );
  return await result.json();
};

export default FirstPageHelper;

